package com.malconectado.birt.process;

import java.io.File;
import java.util.ArrayList;

import org.adempiere.exceptions.AdempiereException;
import org.adempiere.webui.apps.AEnv;
import org.adempiere.webui.component.Window;
import org.adempiere.webui.part.WindowContainer;
import org.adempiere.webui.session.SessionManager;
import org.compiere.model.MAttachment;
import org.compiere.model.MAttachmentEntry;
import org.compiere.model.MProcess;
import org.compiere.model.MSysConfig;
import org.compiere.print.PrintData;
import org.compiere.print.ReportEngine;
import org.compiere.process.ProcessInfoParameter;
import org.compiere.process.SvrProcess;
import org.zkoss.zk.ui.Executions;

import com.malconectado.birt.form.BirtReportViewer;
/**
 * Birt report prcess default
 * @author jcastillo
 *
 */
public class BirtReportProcess extends SvrProcess {

	
	ArrayList<ProcessInfoParameter> params = new ArrayList<>();
	@Override
	protected void prepare() {
		
		ProcessInfoParameter[] para = getParameter();
		for (int i = 0; i < para.length; i++)
		{
			
			if (para[i].getParameter() != null) {
				params.add(para[i]);
			}
				
		}

	}

	@Override
	protected String doIt() throws Exception {	
		//http://localhost:8081/birt/frameset?__report=report/testReport.rptdesign&AD_User_ID=1000000
		
		MProcess process = MProcess.get(getCtx(), getProcessInfo().getAD_Process_ID());
		String reportName = process.getJasperReport();
		String birtHome = MSysConfig.getValue("birt.report.path", "\report", getAD_Client_ID());
		
		String localName = birtHome + System.getProperty("file.separator") + reportName;
		String birtHost = MSysConfig.getValue("birt.report.host","http://localhost:8081/birtReportFiles/frameset?__report=", getAD_Client_ID());
		if (birtHome == null) {
			return "@Error@ Not definied birt.report.host";
		}
		
		//put file in birt folder
		MAttachment attachment = process.getAttachment();
		if (attachment != null) {
			MAttachmentEntry[] entries = attachment.getEntries();
			MAttachmentEntry entry = null;
			File reportFile;
			for (int i = 0; i < entries.length; i++) {
				if (entries[i].getName().equals(reportName)) {
					entry = entries[i];
					
					if (entry != null) {

						reportFile = entry.getFile(reportName);	
						File localReportFile = new File(localName);
						boolean createOrMove = false;
						
						//refactor equals logic					
						if (localReportFile.exists() 
								&& localReportFile.lastModified() == reportFile.lastModified()) {
							log.info("No se ha modificado.");							
						} else if (localReportFile.exists() 
								&& localReportFile.lastModified() != reportFile.lastModified()){
							boolean r = localReportFile.delete();
							createOrMove = true;
						}else {
							createOrMove = true;
						}

						if (createOrMove) {	//move to birt dir					
							if (!reportFile.renameTo(new File(localName))) {
								addLog("No Movido");
							}
							
						}				
						
					}				
					
					break;
				}
			}
			
			
		}
		
		//format url
		StringBuilder url = new StringBuilder(birtHost);
		url.append(reportName);
		//url.append("?");
		boolean first = false;
		for (ProcessInfoParameter p : params) {
			if (!first) {
				url.append("&");
			}
			first = false;
			url.append(p.getParameterName())
			.append("=")
			.append(p.getParameter());
			if (p.getParameter_To()!=null) {
				url.append("&").append(p.getParameterName()).append("_To")
				.append("=")
				.append(p.getParameter_To());
			}
		}
		/*if (!first) {
			url.append("&");
		}
		
		url.append("AD_CLIENT_ID=").append(getProcessInfo().getAD_Client_ID());
		url.append("&").append("AD_PINSTANCE_ID=").append(getProcessInfo().getAD_PInstance_ID());
		url.append("&").append("AD_USER_ID=").append(getProcessInfo().getAD_User_ID());
		url.append("&").append("RECORD_ID=").append(getRecord_ID());*/
		addLog(url.toString());
		
		
		
		//addLog("<html><iframe src = \"https://twitter.com\"  width = \"100%\" height = \"700\" > test <\\iframe></html>");
		Window viewer = new BirtReportViewer(null, url.toString());
		
    	viewer.setAttribute(Window.MODE_KEY, Window.MODE_EMBEDDED);
    	viewer.setAttribute(Window.INSERT_POSITION_KEY, Window.INSERT_NEXT);
    	viewer.setAttribute(WindowContainer.DEFER_SET_SELECTED_TAB, Boolean.TRUE);
    	viewer.setTitle(process.get_Translation("Name"));
		if (Executions.getCurrent() != null){
			
	    	SessionManager.getAppDesktop().showWindow(viewer);
		}else {
			AEnv.executeAsyncDesktopTask(new Runnable() {			
				@Override
				public void run() {
			    	SessionManager.getAppDesktop().showWindow(viewer);			
				}
			});
		}
		return null;
	}

}

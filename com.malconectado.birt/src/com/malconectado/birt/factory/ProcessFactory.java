/**
 * 
 */
package com.malconectado.birt.factory;

import org.adempiere.base.IProcessFactory;
import org.compiere.process.ProcessCall;

/**
 * @author jcastillo
 *
 */
public class ProcessFactory implements IProcessFactory {
	private static String PACKAGE_NAME = "com.malconectado.birt.process";
	/* (non-Javadoc)
	 * @see org.adempiere.base.IProcessFactory#newProcessInstance(java.lang.String)
	 */
	@Override
	public ProcessCall newProcessInstance(String className) {
		if (!className.startsWith(PACKAGE_NAME)) {
			return null;
		}
		
		try {
			Class<? extends ProcessCall> clasz = (Class<? extends ProcessCall>)Class.forName(className);
			return clasz.newInstance();
		} catch (ClassNotFoundException e) {
		} catch (InstantiationException e) {
		} catch (IllegalAccessException e) {
		}
		
		return null;
	}

}

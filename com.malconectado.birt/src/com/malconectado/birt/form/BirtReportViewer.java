package com.malconectado.birt.form;

import org.adempiere.webui.component.Window;
import org.compiere.print.ReportEngine;
import org.zkoss.zul.Borderlayout;
import org.zkoss.zul.Center;
import org.zkoss.zul.Iframe;

public class BirtReportViewer extends Window {

	/**
	 * 
	 */
	private static final long serialVersionUID = 6111015426699468083L;

	public BirtReportViewer(ReportEngine re, String url) {
		Borderlayout layout = new Borderlayout();
		layout.setStyle("position: relative; height: 100%; width: 100%; border:none; padding:none; margin:none;");
		this.appendChild(layout);
		Iframe i = new Iframe(url);
		i.setStyle("height: 100%; width: 100%;");
		Center center = new Center();
		//center.setAutoscroll(true);
		center.setStyle("height: 97%; width: 98%;");
		center.appendChild(i);
		
		layout.appendChild(center);
	}
}

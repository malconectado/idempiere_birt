package com.malconectado.birt.form;

import org.adempiere.webui.apps.AEnv;
import org.adempiere.webui.component.Window;
import org.adempiere.webui.part.WindowContainer;
import org.adempiere.webui.session.SessionManager;
import org.adempiere.webui.window.ZkReportViewerProvider;
import org.compiere.model.MProcess;
import org.compiere.print.ReportEngine;
import org.compiere.print.ReportViewerProvider;
import org.zkoss.zk.ui.Executions;

public class BirtReportViewerProvider implements ReportViewerProvider{

	@Override
	public void openViewer(ReportEngine re) {
		MProcess p = MProcess.get(re.getCtx(), re.getPrintInfo().getAD_Process_ID());
		if (p.get_ValueAsString("BirtReport")!=null && !p.get_ValueAsString("BirtReport").trim().isEmpty()) {
			
			if (Executions.getCurrent() != null){
				Window viewer = new BirtReportViewer(re,"");
				
		    	viewer.setAttribute(Window.MODE_KEY, Window.MODE_EMBEDDED);
		    	viewer.setAttribute(Window.INSERT_POSITION_KEY, Window.INSERT_NEXT);
		    	viewer.setAttribute(WindowContainer.DEFER_SET_SELECTED_TAB, Boolean.TRUE);
		    	SessionManager.getAppDesktop().showWindow(viewer);
			}else {
				AEnv.executeAsyncDesktopTask(new Runnable() {			
					@Override
					public void run() {
						Window viewer = new BirtReportViewer(re,"");
						
				    	viewer.setAttribute(Window.MODE_KEY, Window.MODE_EMBEDDED);
				    	viewer.setAttribute(Window.INSERT_POSITION_KEY, Window.INSERT_NEXT);
				    	viewer.setAttribute(WindowContainer.DEFER_SET_SELECTED_TAB, Boolean.TRUE);
				    	SessionManager.getAppDesktop().showWindow(viewer);			
					}
				});
			}			
			
			
		}else {
			new ZkReportViewerProvider().openViewer(re);
		}
	}

}
